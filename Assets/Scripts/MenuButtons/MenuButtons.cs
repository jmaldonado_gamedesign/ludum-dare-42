﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine;

public class MenuButtons : MonoBehaviour {

    GameObject AboutUs;
    GameObject AboutUsButton;

    void Start()
    {

        AboutUs = GameObject.Find("Canvas").transform.GetChild(0).gameObject;
        AboutUsButton = GameObject.Find("Canvas").transform.GetChild(3).gameObject;
        
    }


    public void ActiveAboutUs()
    {
        if (AboutUs.activeInHierarchy)
        {

            AboutUs.SetActive(false);
        }
        else
        {
            AboutUs.SetActive(true);
        }

        EventSystem.current.SetSelectedGameObject(null);
    }

    public void LoadLevelByIndex(int index)
    {
        EventSystem.current.SetSelectedGameObject(null);
        SceneManager.LoadScene(index);
    }

    public void ExitApplication()
    {
        EventSystem.current.SetSelectedGameObject(null);
        Application.Quit();
    }
}
