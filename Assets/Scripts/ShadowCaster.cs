﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowCaster : MonoBehaviour {

    Sprite spriteShadow;
    GameObject shadow;
    Vector3 temp = new Vector3(0,0,0);

   

    void Start()
    {
        spriteShadow = gameObject.GetComponent<SpriteRenderer>().sprite;
        shadow = new GameObject();
        shadow.transform.position = gameObject.transform.position;
        //sprite
        shadow.AddComponent<SpriteRenderer>();
        shadow.GetComponent<SpriteRenderer>().sprite = spriteShadow;
        shadow.GetComponent<SpriteRenderer>().color = Color.black;
        shadow.GetComponent<SpriteRenderer>().sortingOrder = -50;
        shadow.transform.parent = gameObject.transform;
        
        shadow.transform.name = gameObject.transform.name + "Shadow";
        shadow.transform.Translate(new Vector3(0.15F,-0.15F, 0.0F));
        shadow.transform.localScale = new Vector3(1, 1, 1);
        shadow.transform.localRotation = Quaternion.Euler(0, 0, 0);


    }

    // Update is called once per frame
    void Update () {
        
    }
}
