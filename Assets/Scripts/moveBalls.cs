﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBalls : MonoBehaviour {

    //float pointToMove;
    //float middlePoint;
    [SerializeField]
    public float time;
    public bool makeArea;

    private float middleTime;
    private float timer;
    private float velocity;
    public float angle;
    public bool moving;
    public Vector3 initialPosition;   



    // Use this for initialization
    void Start () {
        //pointToMove = transform.position.x + 10;
        //middlePoint = (transform.position.x + pointToMove) / 2;
        timer = 0;
        middleTime = time / 2;
        moving = false;
        makeArea = false;
        velocity = 0.1f;
        initialPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

        
       
        //Efecto parabola

        if (timer < time && moving) {
            timer += Time.deltaTime;
            //transform.position = new Vector3(transform.position.x + 0.1f, transform.position.y, transform.position.z);
            //transform.position = new Vector3(transform.position.x, transform.position.y + 0.01f, transform.position.z);
            //transform.Translate(VectorFromAngle(angle));
            /*transform.rotation = Quaternion.Euler(0,0,angle);
            transform.position = Vector3.back * velocity;*/

            transform.position = new Vector3(transform.position.x + (Mathf.Cos(AngleToRad(angle)) * velocity), transform.position.y + (Mathf.Sin(AngleToRad(angle)) * velocity), transform.position.z);

            /*if (timer < middleTime) {
                transform.localScale = new Vector3(transform.localScale.x + 0.01f, transform.localScale.y + 0.01f, transform.localScale.z + 0.01f);
            }
            else {
                transform.localScale = new Vector3(transform.localScale.x - 0.01f, transform.localScale.y - 0.01f, transform.localScale.z - 0.01f);

            }*/
        }
        else if(timer > time){
            moving = false;
            makeArea = true;
            timer = 0;
            //transform.localScale = new Vector3(1, 1, 1);
        }

    }

    float AngleToRad(float newAngle){
        float rad = (newAngle * 2 * Mathf.PI) / 360; 
        return rad;
    }

    void OnTriggerEnter2D (Collider2D collider)
    {
        if( collider.gameObject.tag == "up line" || collider.gameObject.tag == "down line")
        {
            angle = 360 - angle;
        }

        if (collider.gameObject.tag == "right line" || collider.gameObject.tag == "left line")
        {
            angle = 180 - angle;
        }
  
    }

}
