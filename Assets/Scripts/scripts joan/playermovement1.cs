﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playermovement1 : MonoBehaviour  {

    private Rigidbody2D RigidbodyPlayer1;

    [SerializeField]
    private float movementSpeed;
    private GameObject ball;
    private int player1 = 3;
    public bool controlScorePlayer1;

    GameObject HitTarget;

    bool right = false;
    bool left = false;
    bool up = false;
    bool down = false;

    bool equippedBall = false;
    bool goingToThrow = false;
    bool increse = true;
    bool inmortality = false;
    int movementNum = 1;
    float rotationTarget = 0;
    float horizontal;
    float vertical;
    float blinkTimer;
    float timerInmortality;
    Quaternion temp;

    public Vector3 initialPosition;

    //ball timer until it can be grabbed again
    float timerUntilGrabbingBall = 0;

    //icyfloor bool
    private bool icyFloor = false; 

    //audio sources
    public AudioClip ThrowballSound;
    public AudioClip HurtSound;
    AudioSource AudioPlayer;

    // Use this for initialization
    void Start () {
        RigidbodyPlayer1 = GetComponent<Rigidbody2D>();
        HitTarget = gameObject.transform.GetChild(0).gameObject;
        horizontal = 0;
        vertical = 0;
        blinkTimer = 0;
        timerInmortality = 0;
        initialPosition = transform.position;
        controlScorePlayer1 = false;
        AudioPlayer = transform.GetComponent<AudioSource>();
       

    }
	
	// Update is called once per frame
	void Update () {
        //Inmortality
        if (inmortality){
            blink();
            timerInmortality += Time.deltaTime;
        }
        if(timerInmortality > 2){
            inmortality = false;
            blinkTimer = 0;
            timerInmortality = 0;
            GetComponent<SpriteRenderer>().enabled = true;
            transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
        }

        // Debug.Log(equippedBall); 
        if (Input.GetKeyUp(KeyCode.E)){
            if (equippedBall){
                ball.GetComponent<moveBalls>().moving = true;
                ball.GetComponent<moveBalls>().angle = gameObject.transform.GetChild(0).transform.eulerAngles.z;
                Debug.Log("Angle: " + gameObject.transform.GetChild(0).transform.eulerAngles.z);
            }
        }
            
        if (!icyFloor) {
            horizontal = Input.GetAxis("Horizontal2");
            vertical = Input.GetAxis("Vertical2");
            if (horizontal < 0) {
                left = true;
                right = false;
            }
            else if (horizontal > 0) {
                right = true;
                left = false;
            }

            else {
                right = false;
                left = false;
            }

            if (vertical < 0) {
                down = true;
                up = false;
            }

            else if (vertical > 0) {
                up = true;
                down = false;
            }

            else {
                up = false;
                down = false;
            }
        }

        

        /*if (player1 <= 0) {
            Destroy(gameObject);
        }*/

        movement(horizontal,vertical);

        

        //INPUTS___________________________________________________________________________________________________________________________________

        if (Input.GetKey(KeyCode.E)) {

            if (equippedBall){
                HitTarget.SetActive(true);
                goingToThrow = true;

            }
        }
        else {
            if (equippedBall && goingToThrow) {
                HitTarget.SetActive(false);
                AudioPlayer.clip = ThrowballSound;
                AudioPlayer.Play();
                gameObject.transform.GetChild(2).transform.parent = null;
                goingToThrow = false;
                equippedBall = false;
                timerUntilGrabbingBall = 1;
            }

        
        }


        //CONDITIONALS______________________________________________________________________________________________________________________________


        if (goingToThrow) {
            if(HitTarget.transform.rotation.eulerAngles.z > 180 && HitTarget.transform.rotation.eulerAngles.z < 270) {
                //Debug.Log(HitTarget.transform.rotation.eulerAngles.z);
                movementNum = 1;
                
            }
            else if(HitTarget.transform.rotation.eulerAngles.z < 180 && HitTarget.transform.rotation.eulerAngles.z > 90) {
                movementNum = -1;
                
            }


            rotationTarget += movementNum;
            HitTarget.transform.rotation = Quaternion.Euler(0, 0, rotationTarget);


        }
        else {
            HitTarget.transform.rotation = Quaternion.Euler(0,0,0);
            movementNum = 1;
            rotationTarget = 0;
            
        }


        //ball staying
        if (equippedBall) {
            Debug.Log("Entro");
            gameObject.transform.GetChild(2).transform.localPosition = new Vector3(1, 0, 0);
        }
        //grab timer
        if (timerUntilGrabbingBall > 0) {
            timerUntilGrabbingBall -= Time.deltaTime;
        }
    }

    void fixedUpdate() {
        if (icyFloor) {
            horizontal = 0;
            vertical = 0;
            if (up) {
                transform.position = new Vector3(transform.position.x, transform.position.y + 0.4f, transform.position.z);
            }
            else if (down) {
                transform.position = new Vector3(transform.position.x, transform.position.y - 0.4f, transform.position.z);
            }
            if (right) {
                transform.position = new Vector3(transform.position.x + 0.4f, transform.position.y, transform.position.z);
            }
            else if (left) {
                transform.position = new Vector3(transform.position.x - 0.4f, transform.position.y, transform.position.z);
            }
        }
    }



    private void movement(float horizontal, float vertical) {
        RigidbodyPlayer1.velocity = new Vector2(horizontal * movementSpeed, RigidbodyPlayer1.velocity.y);

        RigidbodyPlayer1.velocity = new Vector2(RigidbodyPlayer1.velocity.x, vertical * movementSpeed);
    }



    private void OnTriggerEnter2D(Collider2D collision){
        if (collision.gameObject.tag == "Ball" && !collision.gameObject.GetComponent<moveBalls>().moving) {//and the ball is in the relax state
            if (timerUntilGrabbingBall <= 0) {
                if (!equippedBall) {
                    ball = collision.gameObject;
                    collision.gameObject.transform.parent = gameObject.transform;
                    collision.gameObject.transform.localPosition = Vector3.zero;
                    collision.gameObject.transform.localPosition = new Vector3(1, 0, 0);
                    collision.gameObject.GetComponent<CircleCollider2D>().isTrigger = true;
                    equippedBall = true;
                }
            }
        }
        else if (collision.gameObject.tag == "Ball" && collision.gameObject.GetComponent<moveBalls>().moving) {
            if (!inmortality) { 
                player1 = player1 - 1;
                transform.position = initialPosition;
                controlScorePlayer1 = true;
                inmortality = true;
                AudioPlayer.clip = HurtSound;
                AudioPlayer.Play();
            }
        }

        if (collision.gameObject.tag == "fireZone") {
            if (!inmortality) {
                player1 = player1 - 1;
                transform.position = initialPosition;
                controlScorePlayer1 = true;
                inmortality = true;
                AudioPlayer.clip = HurtSound;
                AudioPlayer.Play();
            }
        }

        if (collision.gameObject.tag == "deathBall") {
            if (!inmortality) {
                player1 = player1 - 1;
                transform.position = initialPosition;
                controlScorePlayer1 = true;
                inmortality = true;
                AudioPlayer.clip = HurtSound;
                AudioPlayer.Play();
            }
        }

        if (collision.gameObject.tag == "IceZone") {
            if(horizontal != 0 || vertical != 0){
                icyFloor = true;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D col){
        icyFloor = false;
    }

    private void blink() {
        if(blinkTimer > 0.25f){
            GetComponent<SpriteRenderer>().enabled = false;
            transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
            increse = false;
        }

        else if(blinkTimer <= 0){
            GetComponent<SpriteRenderer>().enabled = true;
            transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
            increse = true;
        }

        if (increse) {
            blinkTimer += Time.deltaTime;
        }
        else {
            blinkTimer -= Time.deltaTime;
        }
    }
}

