﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playermovement2 : MonoBehaviour {

    private Rigidbody2D RigidbodyPlayer2;

    [SerializeField]
    private float movementSpeed;
    private GameObject ball;
    private int player2 = 3;

    public bool controlScorePlayer2;
    GameObject HitTarget;
    float horizontal = 0;
    float vertical = 0;

    bool left = false;
    bool right = false;
    bool up = false;
    bool down = false;
    bool icyFloor = false;
    bool equippedBall = false;
    bool goingToThrow = false;
    bool inmortality = false;
    bool increse = true;

    int movementNum = 1;
    float rotationTarget = 0;
    float blinkTimer = 0;
    float timerInmortality = 0;
    Quaternion temp;

    public Vector3 initialPosition;

    //ball timer until it can be grabbed again
    float timerUntilGrabbingBall = 0;

    //audio sources
    public AudioClip ThrowballSound;
    public AudioClip HurtSound;
    AudioSource AudioPlayer;

    // Use this for initialization
    void Start(){
        RigidbodyPlayer2 = GetComponent<Rigidbody2D>();
        HitTarget = gameObject.transform.GetChild(0).gameObject;
        initialPosition = transform.position;
        controlScorePlayer2 = false;
        AudioPlayer = transform.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {
        //Inmortality
        if (inmortality) {
            blink();
            timerInmortality += Time.deltaTime;
        }
        if (timerInmortality > 2) {
            inmortality = false;
            blinkTimer = 0;
            timerInmortality = 0;
            GetComponent<SpriteRenderer>().enabled = true;
            transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
        }

        if (Input.GetKeyUp(KeyCode.RightShift)) {
            if (equippedBall){
                ball.GetComponent<moveBalls>().moving = true;
                ball.GetComponent<moveBalls>().angle = gameObject.transform.GetChild(0).transform.eulerAngles.z;
                Debug.Log("Angle: " + gameObject.transform.GetChild(0).transform.eulerAngles.z);
            }
        }

        if (!icyFloor){
            horizontal = Input.GetAxis("Horizontal");
            vertical = Input.GetAxis("Vertical");
            if (horizontal < 0){
                left = true;
                right = false;
            }
            else if (horizontal > 0){
                right = true;
                left = false;
            }

            else{
                right = false;
                left = false;
            }

            if (vertical < 0) { 
                down = true;
                up = false;
            }

            else if (vertical > 0){
                up = true;
                down = false;
            }

            else{
                up = false;
                down = false;
            }
        }

        if (icyFloor){
            horizontal = 0;
            vertical = 0;
            if (up){
                transform.position = new Vector3(transform.position.x, transform.position.y + 0.3f, transform.position.z);
            }
            else if (down){
                transform.position = new Vector3(transform.position.x, transform.position.y - 0.3f, transform.position.z);
            }
            if (right){
                transform.position = new Vector3(transform.position.x + 0.3f, transform.position.y, transform.position.z);
            }
            else if (left){
                transform.position = new Vector3(transform.position.x - 0.3f, transform.position.y, transform.position.z);
            }
        }

       /* if (player2 <= 0)
        {
            Destroy(gameObject);
        }*/

        movement(horizontal, vertical);



        //Hit Target Mechanic
        if (Input.GetKey(KeyCode.RightShift)) {
            if (equippedBall) {
                HitTarget.SetActive(true);
                goingToThrow = true;

            }
        }
        else {
            if (equippedBall && goingToThrow) {
                HitTarget.SetActive(false);
                //we TRHOW THE BALL!
                //here we instance the ball
                //gameObject.transform.GetChild(2).GetComponent<CircleCollider2D>().isTrigger = false;
                AudioPlayer.clip = ThrowballSound;
                AudioPlayer.Play();
                gameObject.transform.GetChild(2).transform.parent = null;
                goingToThrow = false;
                equippedBall = false;
                timerUntilGrabbingBall = 1;
            }
        }

        if (goingToThrow)
        {
            if (HitTarget.transform.rotation.eulerAngles.z > 270 && HitTarget.transform.rotation.eulerAngles.z < 360)
            {
                movementNum = -1;
               
            }
            else if (HitTarget.transform.rotation.eulerAngles.z < 90 && HitTarget.transform.rotation.eulerAngles.z > 0)
            {
                movementNum = 1;
                
            }


            rotationTarget += movementNum;
            HitTarget.transform.rotation = Quaternion.Euler(0, 0, rotationTarget);


        }
        else
        {
            HitTarget.transform.rotation = Quaternion.Euler(0, 0, 180);
            movementNum = 1;
            rotationTarget = -180;

        }


        //ball staying
        if (equippedBall)
        {
            gameObject.transform.GetChild(2).transform.localPosition = new Vector3(-1, 0, 0);
        }
        //grab timer
        if (timerUntilGrabbingBall > 0)
        {
            timerUntilGrabbingBall -= Time.deltaTime;
        }

    }

    private void movement(float horizontal, float vertical)
    {
        RigidbodyPlayer2.velocity = new Vector2(horizontal * movementSpeed, RigidbodyPlayer2.velocity.y);

        RigidbodyPlayer2.velocity = new Vector2(RigidbodyPlayer2.velocity.x, vertical * movementSpeed);
    }


    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Ball" && !collision.gameObject.GetComponent<moveBalls>().moving) {//and the ball is in the relax state
            if (timerUntilGrabbingBall <= 0) {
                if (!equippedBall) {
                    ball = collision.gameObject;
                    collision.gameObject.transform.parent = gameObject.transform;
                    collision.gameObject.transform.localPosition = Vector3.zero;
                    collision.gameObject.transform.localPosition = new Vector3(-1, 0, 0);
                    collision.gameObject.GetComponent<CircleCollider2D>().isTrigger = true;
                    equippedBall = true;
                }
            }
        }

        else if (collision.gameObject.tag == "Ball" && collision.gameObject.GetComponent<moveBalls>().moving) {
            if (!inmortality) {
                player2 = player2 - 1;
                transform.position = initialPosition;
                controlScorePlayer2 = true;
                inmortality = true;
                AudioPlayer.clip = HurtSound;
                AudioPlayer.Play();
            }
            
        }


        if (collision.gameObject.tag == "fireZone") {
            if (!inmortality) {
                player2 = player2 - 1;
                transform.position = initialPosition;
                controlScorePlayer2 = true;
                inmortality = true;
                AudioPlayer.clip = HurtSound;
                AudioPlayer.Play();
            }
        }

        if (collision.gameObject.tag == "deathBall") {
            if (!inmortality) {
                player2 = player2 - 1;
                transform.position = initialPosition;
                controlScorePlayer2 = true;
                inmortality = true;
                AudioPlayer.clip = HurtSound;
                AudioPlayer.Play();
            }
        }

        if (collision.gameObject.tag == "IceZone") {
            if (horizontal != 0 || vertical != 0) {
                icyFloor = true;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D col){
        icyFloor = false;
    }

    private void blink() {
        if (blinkTimer > 0.25f) {
            GetComponent<SpriteRenderer>().enabled = false;
            transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
            increse = false;
        }

        else if (blinkTimer <= 0) {
            GetComponent<SpriteRenderer>().enabled = true;
            transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
            increse = true;
        }

        if (increse) {
            blinkTimer += Time.deltaTime;
        }
        else {
            blinkTimer -= Time.deltaTime;
        }
    }

}
