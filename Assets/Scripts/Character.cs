﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public Texture2D tex;

    void Awake()
    {
        /*SpriteRenderer sr = gameObject.AddComponent<SpriteRenderer>() as SpriteRenderer;
        transform.position = new Vector3(0.0f, 2.5f, 0.0f);

        Sprite sp = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        sr.sprite = sp;*/

        Collider2D col= gameObject.AddComponent<BoxCollider2D>();
        Rigidbody2D rb = gameObject.AddComponent<Rigidbody2D>();
        rb.bodyType = RigidbodyType2D.Dynamic;
        rb.gravityScale = 0;
    }
    enum Direction { Up, Down, Left, Right };
    Vector3 temp;
    public float movementNum = 0.0F;
    Direction myDirectionX;
    Direction myDirectionY;
    public int collisionX = 0;
    public int collisionY = 0;
    /*
        
    */
    // Use this for initialization
    void Start()
    {
        collisionX = 0;
        collisionY = 0;
    }
    void fixedUpdate()
    {
        
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        
       if (col.gameObject.name == "Wall")
       {        
           if (myDirectionY == Direction.Up)
           {
               collisionY = 1;
               /*temp = transform.position;
               temp.y -= movementNum;
               transform.position = temp;*/
           }
           if (myDirectionY == Direction.Down)
           {
               collisionY = 2;
              /* temp = transform.position;
               temp.y += movementNum;
               transform.position = temp;*/
           }
           if (myDirectionX == Direction.Right)
           {
               collisionX = 1;
               /*temp = transform.position;
               temp.x -= movementNum;
               transform.position = temp;*/
           }
           if (myDirectionX == Direction.Left)
           {
               collisionX = 2;
               /*temp = transform.position;
               temp.x += movementNum;
               transform.position = temp;*/
           }
       }
    }

    // Update is called once per frame
    void Update()
    {

        getControls();
        
    }

    void getControls()
    {
        if (Input.GetKey("w"))
        {
            myDirectionY = Direction.Up;
            if (collisionY!=1)
            {
                collisionY = 0;
                temp = transform.position;
                temp.y += movementNum;
                transform.position = temp;
            }

        }
        if (Input.GetKey("s"))
        {
            myDirectionY = Direction.Down;
            if (collisionY!=2)
            {
                collisionY = 0;
                temp = transform.position;
                temp.y -= movementNum;
                transform.position = temp;
            }
        }
        if (Input.GetKey("d")) {
            myDirectionX = Direction.Right;
            if (collisionX!=1) {
                collisionX = 0;
                temp = transform.position;
                    temp.x += movementNum;
                    transform.position = temp;
                }


        }
        if (Input.GetKey("a"))
        {
            myDirectionX = Direction.Left;
            if (collisionX!=2)
            {
                collisionX = 0;
                temp = transform.position;
                temp.x -= movementNum;
                transform.position = temp;
            }
        }
    }
}