﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plantPower : MonoBehaviour {

    private bool arriveToFinal;

    CircleCollider2D circle;

    Vector3 inicialPosition;

    // Use this for initialization
    void Start () {
        inicialPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {

        arriveToFinal = GetComponent<moveBalls>().makeArea;

        if (arriveToFinal == true) {
            Instantiate(Resources.Load("DamagePlantZone"), transform.position, transform.rotation);
            transform.position = inicialPosition;
            GetComponent<moveBalls>().makeArea = false;
        }
		
	}
}
