﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveDeathBall : MonoBehaviour {

    float angle;
    float velocity;

	// Use this for initialization
	void Start () {
        angle = Random.Range(0, 360);
        if (angle == 0 || angle == 180 || angle == 90 || angle == 360) {
            angle += 10;
        }
        velocity = 0.08f;
    }
	
    void Update() { 
	    // Update is called once per frame
	    transform.position = new Vector3(transform.position.x + (Mathf.Cos(AngleToRad(angle)) * velocity), transform.position.y + (Mathf.Sin(AngleToRad(angle)) * velocity), transform.position.z);
    }

    float AngleToRad(float newAngle) {
        float rad = (newAngle * 2 * Mathf.PI) / 360;
        return rad;
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.tag == "up line" || collider.gameObject.tag == "down line") {
            angle = 360 - angle;
            if(velocity < 0.8f) {
                velocity += 0.0025f;
            }
        }

        if (collider.gameObject.tag == "right line" || collider.gameObject.tag == "left line") {
            angle = 180 - angle;
            if (velocity < 0.8f) {
                velocity += 0.0025f;
            }
        }
    }
}
