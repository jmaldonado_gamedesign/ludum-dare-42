﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManger : MonoBehaviour {

    private int Score1;
    private int Score2;
    private int maxScore;
    private float time;
    private float maxTime;
    private bool controlTime;
    private bool win;
    private bool printWinner;
    private float timeDeathBall;
    private float maxTimeDeathBall;
    private string text;




    public GameObject Player1;
    public GameObject Player2;

    public GameObject textScore1;
    public GameObject textScore2;

   

	// Use this for initialization
	void Start () {
        Score1 = 0;
        Score2 = 0;
        maxScore = 3;
        controlTime = false;
        win = false;
        time = 0.0f;
        maxTime = 2.0f;
        timeDeathBall = 0.0f;
        maxTimeDeathBall = 30.0f;
        text = "";
        printWinner = false;
    }

    // Update is called once per frame
    void FixedUpdate(){
        if (Score1 == maxScore || Score2 == maxScore) {
            Debug.Log("winer");
            checkWinner();
        }

        if (Input.GetKey(KeyCode.Escape))
        {

            Application.Quit();
        }

        if (!win) {
            Debug.Log("In");
            timeDeathBall += Time.deltaTime;
            if(timeDeathBall >= maxTimeDeathBall) {
                GameObject deathBall = Instantiate(Resources.Load("Death Ball"), new Vector3(0, 0, 0), transform.rotation) as GameObject;
                timeDeathBall = 0.0f;
            }
        }



        if (Player1 != null){

            if (Player1.GetComponent<playermovement1>().controlScorePlayer1 == true) {
                AddScorePlayer2();
                Player1.GetComponent<playermovement1>().controlScorePlayer1 = false;
            }
        }

        if (Player2 != null) {

            if (Player2.GetComponent<playermovement2>().controlScorePlayer2 == true) {
                AddScorePlayer1();
                Player2.GetComponent<playermovement2>().controlScorePlayer2 = false;
            }
        }



        if (controlTime) {
            if (GameObject.FindWithTag("deathBall") != null) {
                Destroy(GameObject.FindWithTag("deathBall"));
            }
            time += Time.deltaTime;

            if (time >= maxTime) {
                GameObject deathBall = Instantiate(Resources.Load("Death Ball"), new Vector3(0, 0, 0), transform.rotation) as GameObject;
                Debug.Log("entro");
                textScore1.SetActive(false);
                textScore2.SetActive(false);
                time = 0.0f;
                controlTime = false;
            }
        }

        if (printWinner) {
            Debug.Log(SceneManager.GetActiveScene().name);
            if (SceneManager.GetActiveScene().name == "WinnerScene") {
                GameObject.FindGameObjectWithTag("winnerCanvas").transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = text;
                printWinner = false;
            }
        }

    }

    public void AddScorePlayer1() {
        Score1 = Score1 + 1;
        ShowScore();
    }

    public void AddScorePlayer2() {
        Score2 = Score2 + 1;
        ShowScore();
    }

    //we check the winner
    private void checkWinner() {

        if(Score1 == Score2) {
            //DRAW
            controlTime = false;
            textScore1.SetActive(false);
            textScore2.SetActive(false);
            DontDestroyOnLoad(this);
            SceneManager.LoadScene("WinnerScene");
            Score2 = 0;
            Score1 = 0;
            win = true;
            text = "Draw!!";
        }
        else if(Score2 > Score1) {
            controlTime = false;
            textScore1.SetActive(false);
            textScore2.SetActive(false);
            DontDestroyOnLoad(this);
            SceneManager.LoadScene("WinnerScene");
            Score2 = 0;
            win = true;
            text = "Player2 win!!";
        }
        else {
            //P1 WIN
            //change Scene
            controlTime = false;
            textScore1.SetActive(false);
            textScore2.SetActive(false);
            DontDestroyOnLoad(this);
            SceneManager.LoadScene("WinnerScene");
            Score1 = 0;
            win = true;
            text = "Player1 win!!";
        }
        printWinner = true;
    }

    private void RestartPosition() {

        if (Player1 != null) {
            Player1.transform.position = Player1.GetComponent<playermovement1>().initialPosition;
        }

        if(Player2 != null) {
            Player2.transform.position = Player2.GetComponent<playermovement2>().initialPosition;
        }

        foreach (GameObject item in GameObject.FindGameObjectsWithTag("plantZone")) {
            Destroy(item);
        }

        foreach (GameObject item in GameObject.FindGameObjectsWithTag("fireZone")) {
            Destroy(item);
        }

        foreach (GameObject item in GameObject.FindGameObjectsWithTag("IceZone")) {
            Destroy(item);
        }


    }

    private void ShowScore() {
        textScore1.SetActive(true);
        textScore2.SetActive(true);
        transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = Score1.ToString();
        transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = Score2.ToString();
        controlTime = true;
        RestartPosition();
    }
}
