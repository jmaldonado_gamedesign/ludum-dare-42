﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerAreas : MonoBehaviour {

    private string nameOfTag;

	// Use this for initialization
	void Start () {

        if (gameObject.tag == "plantZone")
        {
            nameOfTag = "IceZone";
        }

        if (gameObject.tag == "IceZone")
        {
            nameOfTag = "fireZone";
        }

        if (gameObject.tag == "fireZone")
        {
            nameOfTag = "plantZone";
        }

    }
	
	// Update is called once per frame
	void Update () {
       
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log(collider.gameObject.tag);
        if (collider.gameObject.tag == nameOfTag)
        {
            Destroy(collider.gameObject);
        }
    }
}
